CREATE DATABASE  IF NOT EXISTS `nju` /*!40100 DEFAULT CHARACTER SET utf8 */;
USE `nju`;
-- MySQL dump 10.13  Distrib 5.6.13, for Win32 (x86)
--
-- Host: localhost    Database: nju
-- ------------------------------------------------------
-- Server version	5.6.17

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `order`
--

DROP TABLE IF EXISTS `order`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `order` (
  `oid` int(255) NOT NULL AUTO_INCREMENT,
  `pname` varchar(255) NOT NULL,
  `pnum` int(11) NOT NULL,
  `per_price` decimal(10,0) NOT NULL,
  `supplier_name` varchar(255) CHARACTER SET utf8 NOT NULL,
  `odate` datetime DEFAULT NULL,
  `first_payment` decimal(10,0) NOT NULL,
  `all_payment` decimal(10,0) NOT NULL,
  PRIMARY KEY (`oid`),
  KEY `supplier_name` (`supplier_name`),
  KEY `supplier_name_2` (`supplier_name`),
  KEY `supplier_name_3` (`supplier_name`),
  KEY `supplier_name_4` (`supplier_name`),
  KEY `supplier_name_5` (`supplier_name`),
  KEY `supplier_name_6` (`supplier_name`),
  KEY `supplier_name_7` (`supplier_name`),
  KEY `supplier_name_8` (`supplier_name`),
  KEY `supplier_name_9` (`supplier_name`),
  KEY `supplier_name_10` (`supplier_name`),
  KEY `supplier_name_11` (`supplier_name`),
  KEY `supplier_name_12` (`supplier_name`),
  KEY `supplier_name_13` (`supplier_name`),
  KEY `pname` (`pname`),
  CONSTRAINT `order_ibfk_1` FOREIGN KEY (`supplier_name`) REFERENCES `supplier` (`supplier_name`),
  CONSTRAINT `order_ibfk_2` FOREIGN KEY (`pname`) REFERENCES `items` (`item_name`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `order`
--

LOCK TABLES `order` WRITE;
/*!40000 ALTER TABLE `order` DISABLE KEYS */;
INSERT INTO `order` VALUES (1,'1',1,1,'1','2017-01-01 00:00:00',1,1);
/*!40000 ALTER TABLE `order` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2018-01-07 15:15:56
