package reportUI;

import java.awt.Font;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.util.ArrayList;

import report.reportImp;
import reportdao.reportdaoImp;

import org.jfree.chart.ChartFactory;
import org.jfree.chart.ChartFrame;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.axis.CategoryAxis;
import org.jfree.chart.axis.NumberAxis;
import org.jfree.chart.axis.ValueAxis;
import org.jfree.chart.plot.CategoryPlot;
import org.jfree.chart.plot.PlotOrientation;
import org.jfree.data.*;
import org.jfree.chart.*;
import org.jfree.data.category.DefaultCategoryDataset;
import org.jfree.data.general.DefaultPieDataset;

public class reportUIImp implements reportUI {
	public void show() {
		reportmainUI a=new reportmainUI();
		a.run();
	}

	public ArrayList daoshow1(String year) {
		reportImp a=new reportImp();
		return a.getyear1(year);
	}

	public ArrayList daoshow2(String year) {
		reportImp a=new reportImp();
		return a.getyear2(year);
	}

	@Override
	public void daoshow12() {
		
	}

}
