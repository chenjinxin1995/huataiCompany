package Frame;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Vector;

import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JSplitPane;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.ListSelectionModel;
import javax.swing.UIManager;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;

import Action.order;
import DefaultTable.orderDefalutTable;
import Form.orders;
import table.orderframe;

public class sale extends JFrame implements ActionListener,ListSelectionListener{
	
	private static final long serialVersionUID = -2000035301302910482L;
	
	//数据库操作模块
	private JLabel id;
	private JTextField idText;
	private JLabel name;
	private JTextField nameText;
	private JLabel num;
	private JTextField numText;
	private JLabel supplierName;
	private JComboBox supplierComboBox;
	private JButton searchButton;
	private JButton createButton;
	private JButton deleteButton;
	private JButton changeButton;
	private JButton clearButton;
	private JButton cancel;
	private Container contentPane;
	private JPanel jPanel1;
	private JSplitPane jSplitPane;
	private JTable jTable;
	private JScrollPane jScrollPane;
	private ListSelectionModel selectionMode;
	
	@SuppressWarnings("unchecked")
	private Vector orders;
	private orders order;
	
	private int selected = 0;
	
	public sale() {
		try {
    //设置显示外观为本地系统外观，注意此句需放在初始化所有控件之前
		UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
		} catch (Exception e) {
			e.printStackTrace();
		}
		initComponents();
	}
	
	private void initComponents() {
		contentPane=getContentPane();
		jPanel1=new JPanel();
		jPanel1.setLayout(null);
		
		
		id=new JLabel();
		id.setText("订单号:");
		id.setFont(new Font("",Font.BOLD,15));
		id.setBounds(10, 20, 100, 20);
		jPanel1.add(id);
		
		idText=new JTextField();
		idText.setBounds(80, 20, 100, 20);
		jPanel1.add(idText);
		
		name=new JLabel();
		name.setText("订单名:");
		name.setFont(new Font("",Font.BOLD,15));
		name.setBounds(210, 20, 100, 20);
		jPanel1.add(name);		
		
		nameText=new JTextField();
		nameText.setBounds(280, 20, 100, 20);
		jPanel1.add(nameText);
		
		num=new JLabel();
		num.setText("数量:");
		num.setFont(new Font("",Font.BOLD,15));
		num.setBounds(410, 20, 100, 20);
		jPanel1.add(num);		
		
		numText=new JTextField();
		numText.setBounds(480, 20, 100, 20);
		jPanel1.add(numText);
		
		supplierName=new JLabel();
		supplierName.setText("供货商:");
		supplierName.setFont(new Font("",Font.BOLD,15));
		supplierName.setBounds(10, 50, 100, 20);
		jPanel1.add(supplierName);
		
		//初始化班级下拉框
		order studentAction = new order();
		String[] supplier = studentAction.getAllSupplierName();
		supplierComboBox=new JComboBox(supplier);
		supplierComboBox.setSelectedItem("");
		supplierComboBox.setBounds(80, 50, 140, 20);
		jPanel1.add(supplierComboBox);
		
		searchButton=new JButton("查询");
		searchButton.setFont(new Font("",Font.BOLD,15));
		searchButton.setBounds(10, 80, 100, 30);
		searchButton.addActionListener(this);
		jPanel1.add(searchButton);
		
		createButton=new JButton("新建");
		createButton.setFont(new Font("",Font.BOLD,15));
		createButton.setBounds(130, 80, 100, 30);
		createButton.addActionListener(this);
		jPanel1.add(createButton);
		
		changeButton=new JButton("修改");
		changeButton.setFont(new Font("",Font.BOLD,15));
		changeButton.setBounds(250, 80, 100, 30);
		//changeButton.setEnabled(false);
		changeButton.addActionListener(this);
		jPanel1.add(changeButton);
		
		deleteButton=new JButton("删除");
		deleteButton.setFont(new Font("",Font.BOLD,15));
		deleteButton.setBounds(10, 120, 100, 30);
		//deleteButton.setEnabled(false);
		deleteButton.addActionListener(this);
		jPanel1.add(deleteButton);
		
		clearButton=new JButton("清空");
		clearButton.setFont(new Font("",Font.BOLD,15));
		clearButton.setBounds(130, 120, 100, 30);
		clearButton.addActionListener(this);
		jPanel1.add(clearButton);
		
		cancel=new JButton("退出");
		cancel.setFont(new Font("",Font.BOLD,15));
		cancel.setBounds(250, 120, 100, 30);
		cancel.addActionListener(this);
		jPanel1.add(cancel);
		
		jTable=new JTable(new orderDefalutTable(15,4));
		jScrollPane=new JScrollPane(jTable);
		
		jSplitPane=new JSplitPane(JSplitPane.VERTICAL_SPLIT,false,jPanel1,jScrollPane);
		jSplitPane.setDividerLocation(180);//表格的相对位置
		jSplitPane.setOneTouchExpandable(true);
		jSplitPane.setDividerSize(5);
		
		contentPane.add(jSplitPane);
		
		//Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
		Dimension screenSize = new Dimension(600, 500);//窗口大小
		this.setPreferredSize(screenSize);
		this.setBounds(screenSize.width/2-50,screenSize.height/2-80,500,500);
		this.setVisible(true);
		this.setTitle("查询界面");
		setResizable(false);
		pack();
	}

	
	//清空输入的查询条件
	private void clear(){
		idText.setText("");
		nameText.setText("");
		numText.setText("");
		supplierComboBox.setSelectedItem("");
	}

	//捕捉到JButton点击事件后的实现的逻辑
	@SuppressWarnings("unchecked")
	public void actionPerformed(ActionEvent e) {
		order stuAction = new order();
		// 点击"退出"按钮
		if(e.getActionCommand().equals("退出")){
			
			this.dispose();
			new DispatchFrame().setVisible(true);
		}
		//点击"清空"按钮
		if(e.getActionCommand().equals("清空")){
			//清空输入的查询条件
			clear();
		}
		//点击"新建"按钮
		if(e.getActionCommand().equals("新建")){
			    //创建并进入新建模块
				//createFrame=new CreateFrame(this);
                if(idText.getText().equals("")||nameText.getText().equals("")
                		||numText.getText().equals("")) {
    				String message="订单号，订单名，数量均不能为空";
    				//显示提示框,提示用户
    				JOptionPane.showMessageDialog(this, message);
                }
                else if(!idText.getText().matches("[1-9][0-9]*")) {
                	String message="学号必须为非零开始的数字";
    				//显示提示框,提示用户
    				JOptionPane.showMessageDialog(this, message);
                }
                else {
                	orders order = new orders();
                	order.setOrder_id(Integer.valueOf(idText.getText()).intValue());
                	order.setOrder_name(nameText.getText());
                	order.setOrder_num(Integer.valueOf(numText.getText()).intValue());
                	order.setSupplier_name(supplierComboBox.getSelectedItem().toString());
                	
    				if(stuAction.isIdUsed(Integer.valueOf(idText.getText()).intValue())) {
    					String message="订单号不可用";
        				//显示提示框,提示用户
        				JOptionPane.showMessageDialog(this, message);
    				}
    				else {
    					if(stuAction.newStudent(order)) {
                    		String message="订单信息新建成功";
            				//显示提示框,提示用户
            				JOptionPane.showMessageDialog(this, message);
            				this.afterCreate(order);
                    	}
    				}                	
                }
		}
		//点击"删除"按钮
		if(e.getActionCommand().equals("删除")){
			//若没有在查询功能界面中输入学号，不允许进入删除界面
			if(idText.getText().equals("")){
				//未输入学号的提示信息
				String message="删除时订单号不能为空";
				//显示提示框,提示用户
				JOptionPane.showMessageDialog(this, message);
			}
			else{
				int order_id = Integer.valueOf(idText.getText()).intValue();
					if(stuAction.deleteOneStudent(order_id)) {
						//未输入学号的提示信息
						String message="成功删除一条记录";
						//显示提示框,提示用户
						JOptionPane.showMessageDialog(this, message);
						this.afterDelete();
					}
					else{
						//未输入学号的提示信息
						String message="未能成功删除一条记录";
						//显示提示框,提示用户
						JOptionPane.showMessageDialog(this, message);
					}			
			}
			
			
		}
		//点击"修改"按钮
		if(e.getActionCommand().equals("修改")){
			if(idText.getText().equals("")||nameText.getText().equals("")
            		||numText.getText().equals("")) {
				String message="订单号，订单名，数量均不能为空";
				//显示提示框,提示用户
				JOptionPane.showMessageDialog(this, message);
            }
			else{
				orders s = new orders();
				s.setOrder_id(Integer.valueOf(idText.getText()).intValue());
				s.setOrder_name(nameText.getText());
				s.setOrder_num(Integer.valueOf(numText.getText()).intValue());
				s.setSupplier_name(supplierComboBox.getSelectedItem().toString());
				if(stuAction.updateOneStudent(s)) {
					String message="更新成功";
					//显示提示框,提示用户
					JOptionPane.showMessageDialog(this, message);
					this.afterChange(s);					
				}
			}
		}
		//点击"查询"按钮
		if(e.getActionCommand().equals("查询")){
			//获得支持模糊查询的SQL语句
			String sql=getQuerySQL();
			System.out.print(sql);
			//获得查询后的结果集
			List tchers = new ArrayList();
			tchers = stuAction.executeQuery(sql);
			//若没有符合条件的记录，清空输入的查询条件
		    if(tchers.isEmpty()){
		    	clear();
		    	//没有查询到符合条件的学生的提示
		    	String message="没有符合条件的订单信息";
		    	//显示提示框,提示用户
				JOptionPane.showMessageDialog(this,message);
		    	return;
		    }		    
		    //将查询记录以Student对象的形式放入查询结果Vector
				orders=new Vector();
				Iterator iterator = tchers.iterator();
				while(iterator.hasNext()) {
					orders.add(iterator.next());
				}
		    //更新查询界面
		    addComponents();
		    //获得代表第一个查询记录的Student对象
		    order=(orders)orders.get(0);
		    //将该对象中的学生信息显示在查询功能界面中
			setStudentInformation(order);
			//将修改和删除按钮设置为可用	
			changeButton.setEnabled(true);			
			deleteButton.setEnabled(true);
		}
		
	}

	//更新查询界面
	private void addComponents() {
		// TODO 自动生成方法存根
		contentPane.removeAll();
		orderframe mt=new orderframe(orders);
		jTable=new JTable(mt);
		selectionMode=jTable.getSelectionModel();
		selectionMode.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
	    selectionMode.addListSelectionListener(this);
        jScrollPane=new JScrollPane(jTable);
		
		jSplitPane=new JSplitPane(JSplitPane.VERTICAL_SPLIT,false,jPanel1,jScrollPane);
		jSplitPane.setDividerLocation(180);
		jSplitPane.setOneTouchExpandable(true);
		jSplitPane.setDividerSize(5);
		contentPane.add(jSplitPane);
		this.setTitle("查询界面");
		this.setVisible(true);
		setResizable(false);
		pack();
	}
	
	//新建学生信息后，更新查询界面
	@SuppressWarnings("unchecked")
	public void afterCreate(orders order){
		if(orders == null){
			orders=new Vector();
		}
		orders.add(order);
		addComponents();
	}
	
	//删除学生信息后，更新查询界面
	public void afterDelete(){
		this.clear();
		orders.remove(selected);
		addComponents();
	}
	
	//修改学生信息后，更新查询界面
	@SuppressWarnings("unchecked")
	public void afterChange(orders t){
		orders.remove(selected);
		orders.add(selected,t);
		addComponents();
	}
	
	//将Student对象包含的学生信息显示在查询功能界面的相应位置上
	private void setStudentInformation(orders Order2){
		idText.setText(Order2.getOrder_id()+"");
		nameText.setText(Order2.getOrder_name());
		numText.setText(Order2.getOrder_num()+"");
		supplierComboBox.setSelectedItem(Order2.getSupplier_name());
	}

	//捕捉ListSelectionEvent事件后的处理逻辑
	public void valueChanged(ListSelectionEvent e) {
		selected = jTable.getSelectedRow();
		orders selectedT = (orders) orders.get(selected);
		this.setStudentInformation(selectedT);
	}
	
    //获得支持模糊查询的SQL语句
	private String getQuerySQL(){
		String[] general={"%","%","%","%"};
		if(!idText.getText().equals("")){
			general[0]="%"+idText.getText()+"%";
		}
		if(!nameText.getText().equals("")){
			general[1]="%"+nameText.getText()+"%";
		}
		String query = "select * from nju.`order` where oid like'"+general[0]+"' and pname like '"+general[1]+"' order by oid";
	    return query;
	}

	

}
