package Frame;

import java.awt.Container;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPasswordField;
import javax.swing.JTextField;
import javax.swing.UIManager;

import Action.LoginAction;


public class LoginFrame extends JFrame implements ActionListener{	
	
	private static final long serialVersionUID = 5214521133987272795L;
	private JLabel name;
	private JLabel pw;
	private JLabel topic;
	private JTextField userText;
	private JPasswordField pwText;
	private JButton login;
	private JButton cancel;
	private Container contentPane;
	
	public LoginFrame() {
		try {
        //设置显示外观为本地系统外观，注意此句需放在初始化所有控件之前
		UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
		} catch (Exception e) {
			e.printStackTrace();
		}
		initComponents();
	}
	//初始化所有控键，并做界面布局
	private void initComponents() {
        //获得ContentPane容器，以便加入各个组件
		contentPane=getContentPane();
        //不设置版面控制，因为将采用绝对位置的方式摆放各个组件
		contentPane.setLayout(null);
        //设置显示"欢迎使用学生管理系统"的JLable控键
		topic=new JLabel();
        //设置JLable显示的内容
		topic.setText("欢迎使用库存管理系统");
	    //设置JLabel的字体，在这里用粗体	
		topic.setFont(new Font("", Font.BOLD, 18));
        //设置JLable的摆放位置和大小
		topic.setBounds(150, 100, 400, 50);
        //把JLable放入容器
		contentPane.add(topic);
		
        //设置显示"用户名"的JLable控键
		name=new JLabel();
		name.setText("用户名");
		name.setFont(new Font("",Font.BOLD,15));      
		name.setBounds(130, 180, 100, 30);
		contentPane.add(name);
		
        //设置输入用户名的文本框
		userText=new JTextField();
		userText.setBounds(200, 180, 150, 30);
		contentPane.add(userText);
		
        //设置显示"密码"的JLable
		pw=new JLabel();
		pw.setText("密码");
		pw.setFont(new Font("",Font.BOLD,15));
		pw.setBounds(130, 230, 100, 30);
		contentPane.add(pw);
		
        //设置输入密码的密码框
		pwText=new JPasswordField();
		pwText.setBounds(200, 230, 150, 30);
		contentPane.add(pwText);
		
        //设置"登录"按钮
		login=new JButton();
		login.setText("登录");
		login.setFont(new Font("",Font.BOLD,15));
        login.setBounds(130, 320, 80, 30);
        //按钮加入监听,有能力捕捉点击事件
        login.addActionListener(this);
        contentPane.add(login);
        
        //设置"退出"按钮
        cancel=new JButton();
        cancel.setText("退出");
        cancel.setFont(new Font("",Font.BOLD,15));
        cancel.setBounds(280, 320, 80, 30);
        //按钮加入监听,有能力捕捉点击事件
        cancel.addActionListener(this);
        contentPane.add(cancel);
        
		//获得屏幕的大小
		Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
		//把窗口放在屏幕中间
		this.setPreferredSize(new Dimension(500, 500));
		this.setBounds(screenSize.width/2-250, screenSize.height/2-250, 500, 500);
        //设置窗口的标题
		this.setTitle("请登录");
        //设置窗口可见
        //不可以改变窗口大小
		this.setResizable(false);
		pack();

		
	}


	/**
	 * @param args
	 */
	public void run() {
				new LoginFrame().setVisible(true);
			
	}

	@SuppressWarnings("deprecation")
	public void actionPerformed(ActionEvent e) {
		// 点击"退出"按钮
		if(e.getActionCommand().equals("退出")){
			//退出系统
			System.exit(0);
		}
		//点击"登录"按钮
		if(e.getActionCommand().equals("登录")){
			LoginAction la = new LoginAction();
			    //判断是否为合法用户
			    if(la.certifyUser(Integer.valueOf(userText.getText()).intValue(), pwText.getText())){
			    	//关闭登录界面
			    	new DispatchFrame();
			    	this.dispose();
			    				    	
			    }
			    else{
			    	//不是合法用户,要求重新输入用户名和密码的提示信息
			    	String message="输入了错误的用户名或密码，请重试";
			    	//显示提示对话框,提示用户
			    	JOptionPane.showMessageDialog(this, message);
			    }
		}
	}

}