package Frame;

import java.awt.Container;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.UIManager;

import mathproblemUI.mathproblemUI;
import reportUI.reportmainUI;
import clientUI.clientmainUI;


public class DispatchFrame extends JFrame implements ActionListener{	
	
	private static final long serialVersionUID = 5214521133987272795L;
	private JLabel topic;	
	private JButton store;
	private JButton student;
	private JButton input;
	private JButton items;
	private JButton cancel;
	
	private JFrame classCourseFrame;
	private JFrame classFrame;
	private JFrame courseFrame;
	private JFrame optCourseFrame;
	private JFrame studentFrame;
	private JFrame teacherFrame;
	private JFrame tcherCourseFrame;
	
	private Container contentPane;
	
	public DispatchFrame() {
		try {
        //设置显示外观为本地系统外观，注意此句需放在初始化所有控件之前
		UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
		} catch (Exception e) {
			e.printStackTrace();
		}
		initComponents();
	}
	//初始化所有控键，并做界面布局
	private void initComponents() {
        //获得ContentPane容器，以便加入各个组件
		contentPane=getContentPane();
        //不设置版面控制，因为将采用绝对位置的方式摆放各个组件
		contentPane.setLayout(null);
        //设置显示"欢迎使用学生管理系统"的JLable控键
		topic=new JLabel();
        //设置JLable显示的内容
		topic.setText("欢迎使用库存管理系统");
	    //设置JLabel的字体，在这里用粗体	
		topic.setFont(new Font("", Font.BOLD, 18));
        //设置JLable的摆放位置和大小
		topic.setBounds(150, 30, 400, 50);
        //把JLable放入容器
		contentPane.add(topic);
		
        //设置"登录"按钮
		store=new JButton();
		store.setText("库存管理");
		store.setFont(new Font("",Font.BOLD,15));
		store.setBounds(90, 90, 150, 30);
        //按钮加入监听,有能力捕捉点击事件
		store.addActionListener(this);
        contentPane.add(store);

      //设置"登录"按钮
      		store=new JButton();
      		store.setText("客户管理");
      		store.setFont(new Font("",Font.BOLD,15));
      		store.setBounds(90, 210, 150, 30);
              //按钮加入监听,有能力捕捉点击事件
      		store.addActionListener(this);
              contentPane.add(store);
              
              store=new JButton();
        		store.setText("报表");
        		store.setFont(new Font("",Font.BOLD,15));
        		store.setBounds(280, 210, 150, 30);
                //按钮加入监听,有能力捕捉点击事件
        		store.addActionListener(this);
                contentPane.add(store);
        
      //设置"登录"按钮
        student=new JButton();
        student.setText("订单管理");
        student.setFont(new Font("",Font.BOLD,15));
        student.setBounds(280, 90, 150, 30);
        //按钮加入监听,有能力捕捉点击事件
        student.addActionListener(this);
        contentPane.add(student);
        
      //设置"登录"按钮
        input=new JButton();
        input.setText("生产管理");
        input.setFont(new Font("",Font.BOLD,15));
        input.setBounds(90, 150, 150, 30);
        //按钮加入监听,有能力捕捉点击事件
        input.addActionListener(this);
        contentPane.add(input);
        
        input=new JButton();
        input.setText("原料配比");
        input.setFont(new Font("",Font.BOLD,15));
        input.setBounds(90, 270, 150, 30);
        //按钮加入监听,有能力捕捉点击事件
        input.addActionListener(this);
        contentPane.add(input);
        
      //设置"登录"按钮
        items=new JButton();
        items.setText("商品查看");
        items.setFont(new Font("",Font.BOLD,15));
        items.setBounds(280, 150, 150, 30);
        //按钮加入监听,有能力捕捉点击事件
        items.addActionListener(this);
        contentPane.add(items);
  
        cancel=new JButton();
        cancel.setText("退出");
        cancel.setFont(new Font("",Font.BOLD,15));
        cancel.setBounds(175, 360, 150, 30);
        //按钮加入监听,有能力捕捉点击事件
        cancel.addActionListener(this);
        contentPane.add(cancel);
        
		//获得屏幕的大小
		Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
		//把窗口放在屏幕中间
		this.setPreferredSize(new Dimension(500, 500));
		this.setBounds(screenSize.width/2-250, screenSize.height/2-250, 500, 500);
        //设置窗口的标题
		this.setTitle("hello");
        //设置窗口可见
		this.setVisible(true);
        //不可以改变窗口大小
		this.setResizable(false);
		pack();

		
	}


	public void actionPerformed(ActionEvent e) {
		// 点击"退出"按钮
		if(e.getActionCommand().equals("退出")){
			//退出系统
			System.exit(0);
		}
		//点击"登录"按钮
		if(e.getActionCommand().equals("库存管理")){
			if(this.courseFrame != null){
				courseFrame.dispose();
			}
			new storeFrame();
			this.setVisible(false);
		}
        if(e.getActionCommand().equals("订单管理")){
        	if(this.studentFrame != null){
        		studentFrame.dispose();
			}
        	new sale();
        	this.setVisible(false);
		}
        if(e.getActionCommand().equals("生产管理")){
        	if(this.teacherFrame != null){
        		teacherFrame.dispose();
			}
        	new produce();
        	this.setVisible(false);
		}
        if(e.getActionCommand().equals("商品查看")){
        	if(this.classFrame != null){
        		classFrame.dispose();
			}
        	new itemFrame();
        	this.setVisible(false);
		}
        if(e.getActionCommand().equals("客户管理")){
			
        	this.setVisible(false);
			clientmainUI a=new clientmainUI();
			a.run();
		}
        if(e.getActionCommand().equals("报表")){
			
        	this.setVisible(false);
			reportmainUI a=new reportmainUI();
			a.run();
		}
        if(e.getActionCommand().equals("原料配比")){
        	this.setVisible(false);
			mathproblemUI a=new mathproblemUI();
			a.run();
		}
	}
	public static void main(String []args){
	new DispatchFrame().setVisible(true);
	}
}