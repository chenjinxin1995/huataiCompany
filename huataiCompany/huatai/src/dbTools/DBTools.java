package dbTools;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

public class DBTools {	
	Connection conn = null;
	ResultSet rs = null;
	public DBTools() throws ClassNotFoundException{
		Class.forName("com.mysql.jdbc.Driver");		
	}
	
	public Connection getConnection() throws SQLException{
		conn = this.getConnection("jdbc:mysql://localhost:3306/nju?characterEncoding=utf8", "root", "chenjinxin1995");
		return conn;
	}
	
	public Connection getConnection(String url) throws SQLException{
		conn = this.getConnection(url, "root", "123");
		return conn;
	}
	
	public Connection getConnection(String url,String name,String pwd) throws SQLException{
		conn = DriverManager.getConnection(url,name,pwd);
		return conn;
	}
	
	public ResultSet getResult(Connection conn,Statement st,String sql){
		try {
			rs = st.executeQuery(sql);
			return rs;
			
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
	}
	
public ResultSet getResult(Connection conn,PreparedStatement pst){
	    try {
			rs = pst.executeQuery();
			return rs;
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
	}
	
	public void closeConnection(Connection conn){
		try{
			if(conn != null) conn.close();
		}catch(Exception e){
			e.printStackTrace();
		}
	}
	
	public void closeResultSet(ResultSet rs){
		try{
			if(rs != null) rs.close();
		}catch(Exception e){
			e.printStackTrace();
		}
	}
	
	public void closeStatement(Statement st){
		try{
			if(st != null) st.close();
		}catch(Exception e){
			e.printStackTrace();
		}
	}
	
	public void closeAllAboutDB(Connection conn,ResultSet rs,Statement st){
		try{
			if(st != null) st.close();
		}catch(Exception e){
			e.printStackTrace();
		}finally{
			try{
				if(rs != null) rs.close();
			}catch(Exception e){
				e.printStackTrace();
			}finally{
				try{
					if(conn != null) conn.close();
				}catch(Exception e){
					e.printStackTrace();
				}				
			}
			
		}
	}
	

	
}
