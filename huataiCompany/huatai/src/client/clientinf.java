package client;

import java.util.Vector;

public class clientinf {
	private String name;
	private int contact;
	private double money;
	private double price;


	public String getName() {
		return name;
	}


	public void setName(String name) {
		this.name = name;
	}


	public int getContact() {
		return contact;
	}


	public void setContact(int contact) {
		this.contact = contact;
	}


	public double getMoney() {
		return money;
	}


	public void setMoney(double money) {
		this.money = money;
	}


	public double getPrice() {
		return price;
	}


	public void setPrice(double price) {
		this.price = price;
	}


	public Vector toVector() {
		Vector v = new Vector(4);
        v.add(0, this.getName());
        v.add(1, this.getContact());
        v.add(2,this.getMoney());
        v.add(3,this.getPrice());
        return v;
	}
}
