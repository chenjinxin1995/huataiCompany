package clientdao;

import java.awt.List;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;

import client.client;
import client.clientinf;
import dbTools.DBTools;


//import dbTools.DBTools;

public class clientdaoImp implements clientdao{


	private PreparedStatement pst;



	public boolean addsql(String name,String contace,String sign,String note) {
		try{
			int contact=Integer.parseInt(contace);
			DBTools dbTools = new DBTools();
			Connection conn = dbTools.getConnection();
			if(sign=="0"){
			String sql="INSERT INTO nju.purchaser (`purchaser_name`, `tel_number`, `remain_payment`, `latter_price`, `note`) VALUES (?,?,?,?,?);";
			pst = conn.prepareStatement(sql);			
			pst.setString(1, name);
			pst.setInt(2, contact);
			pst.setString(3, null);
			pst.setDate(4, null);
			pst.setString(5, note);
			pst.executeUpdate();
			}
			else{
				String sql="INSERT INTO nju.supplier (`supplier_name`, `tel_number`, `remain_payment`, `latter_price`, `note`) VALUES (?,?,?,?,?);";
				pst = conn.prepareStatement(sql);			
				pst.setString(1, name);
				pst.setInt(2, contact);
				pst.setString(3, null);
				pst.setDate(4, null);
				pst.setString(5, note);
				pst.executeUpdate();
			}
			return true;
		}
		catch(Exception e) {
			System.out.print(e);
			return false;
		}
	}




	public ArrayList searchsql() {

		try{
			ArrayList Client = new ArrayList();
			DBTools dbTools = new DBTools();
			Connection conn = dbTools.getConnection();
			Statement pst = conn.prepareStatement("select * from purchaser union select * from supplier");
			ResultSet rs = pst.executeQuery("select * from purchaser union select * from supplier");
			while(rs.next()) {
				clientinf t = new clientinf();
				t.setName(rs.getString(1));
				t.setContact( rs.getInt("tel_number"));
				t.setMoney(rs.getDouble("remain_payment"));
				t.setPrice(rs.getDouble("latter_price"));
				Client.add(t);
			}
			return Client;
		}catch(Exception e) {
			e.printStackTrace();
		}
		return null;
		
	}



	public boolean repay1(String name,String money) {
		try{
			DBTools dbTools = new DBTools();
			Connection conn = dbTools.getConnection();
			String sql = "update purchaser set remain_payment=remain_payment-? where purchaser_name=?";
			pst = conn.prepareStatement(sql);
			pst.setDouble(1, Double.parseDouble(money));
			pst.setString(2, name);
			pst.executeUpdate();
			return true;
		}
		catch(Exception e) {
			return false;
		}
	}

	public boolean repay2(String name,String money) {
		try{
			DBTools dbTools = new DBTools();
			Connection conn = dbTools.getConnection();
			String sql = "update supplier set remain_payment=remain_payment-? where supplier_name=?";
			pst = conn.prepareStatement(sql);
			pst.setDouble(1, Double.parseDouble(money));
			pst.setString(2, name);
			pst.executeUpdate();
			return true;
		}
		catch(Exception e) {
			return false;
		}
	}

	public boolean changesql(String name, String contact, String note,
			String sign, String money, String price) {
		try{
			DBTools dbTools = new DBTools();
			Connection conn = dbTools.getConnection();
			String sql1 = "update nju.supplier set tel_number=? , remain_payment=? ,latter_price=? , note=? where supplier_name=?";
			pst = conn.prepareStatement(sql1);
			pst.setInt(1, Integer.parseInt(contact));
			pst.setDouble(2, Double.parseDouble(money));
			pst.setDouble(3, Double.parseDouble(price));
			pst.setString(4, note);
			pst.setString(5, name);
			pst.executeUpdate();
			
			return true;
		}
		catch(Exception e) {
			return false;
		}
	}
	
	

}
