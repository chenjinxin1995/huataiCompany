package Action;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

import dbTools.DBTools;

public class LoginAction {
	DBTools dbTools = null;
	Connection conn;
	PreparedStatement pst;
	ResultSet rs;
	String sql="";
	public boolean certifyUser(int id,String password) {
		try{
			dbTools = new DBTools();
			conn = dbTools.getConnection();
			sql = "select * from admin where admin_id=? and password=?";
			pst = conn.prepareStatement(sql);
			pst.setInt(1, id);
			pst.setString(2, password);
			rs = pst.executeQuery();
			if(rs.next()) {
				return true;
			}
		}catch(Exception e) {
			e.printStackTrace();
			dbTools.closeAllAboutDB(conn, rs, pst);			
		}		
		return false;
	}
}
