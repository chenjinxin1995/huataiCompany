package Action;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

import Form.orders;
import dbTools.DBTools;

public class order {
	DBTools dbTools = null;
	Connection conn;
	PreparedStatement pst;
	ResultSet rs;
	String sql="";
	
	public orders getOneStudent(int id) {
		try{
			dbTools = new DBTools();
			conn = dbTools.getConnection();
			sql = "select order where oid=?";
			pst = conn.prepareStatement(sql);
			pst.setInt(1, id);
			rs = pst.executeQuery();
			orders order = new orders();
			if(rs.next()) {				
				order.setOrder_id(rs.getInt("oid"));
				order.setOrder_name(rs.getString("pname"));
				order.setOrder_num(rs.getInt("pnum"));
				order.setSupplier_name(rs.getString("supplier_name"));
			}
			return order;
		}catch(Exception e) {
			e.printStackTrace();
			return null;
		}
	}
	
	public boolean deleteOneStudent(int id) {
		try{
			dbTools = new DBTools();
			conn = dbTools.getConnection();
			sql = "delete from `order` where oid=?";
			pst = conn.prepareStatement(sql);
			pst.setInt(1, id);
			if(pst.executeUpdate() != 0) {
				return true;
			}			
		}catch(Exception e) {
			return false;					
		}finally{
			dbTools.closeAllAboutDB(conn, rs, pst);	
		}
		return false;
	}
	
	public boolean updateOneStudent(orders s) {
		try{
			dbTools = new DBTools();
			conn = dbTools.getConnection();
			sql = "update `order` set pname=?,pnum=?,supplier_name=? where oid=?";
			pst = conn.prepareStatement(sql);
			pst.setString(1, s.getOrder_name());
			pst.setInt(2, s.getOrder_num());
			pst.setString(3, s.getSupplier_name());
			pst.setInt(4, s.getOrder_id());
			if(pst.executeUpdate() != 0) {
				return true;
			}
		}catch(Exception e) {
			e.printStackTrace();
			dbTools.closeAllAboutDB(conn, rs, pst);			
		}		
		return false;
	}
	
	@SuppressWarnings("unchecked")
	public List executeQuery(String sql) {
		try{
			List orders = new ArrayList();
			dbTools = new DBTools();
			conn = dbTools.getConnection();
			pst = conn.prepareStatement(sql);
			rs = pst.executeQuery(sql);
			while(rs.next()) {
				orders s = new orders();
				s.setOrder_id(rs.getInt("oid"));
				s.setOrder_name(rs.getString("pname"));
				s.setOrder_num(rs.getInt("pnum"));
				s.setSupplier_name(rs.getString("supplier_name"));
				orders.add(s);
			}
			return orders;
		}catch(Exception e) {
			e.printStackTrace();
		}
		return null;
	}
	
	public boolean newStudent(orders s) {
		try{
			dbTools = new DBTools();
			conn = dbTools.getConnection();
			sql = "insert into `order`(oid,pname,pnum,supplier_name)values(?,?,?,?)";
			pst = conn.prepareStatement(sql);
			pst.setInt(1, s.getOrder_id());
			pst.setString(2, s.getOrder_name());
			pst.setInt(3, s.getOrder_num());
			pst.setString(4, s.getSupplier_name());
			if(pst.executeUpdate() != 0) {
				return true;
			}
		}catch(Exception e) {
			e.printStackTrace();
			dbTools.closeAllAboutDB(conn, rs, pst);			
		}		
		return false;
	}
	
	public boolean isIdUsed(int id) {
		try{
			dbTools = new DBTools();
			conn = dbTools.getConnection();
			sql = "select * from `order` where oid=?";
			pst = conn.prepareStatement(sql);
			pst.setInt(1, id);
			rs = pst.executeQuery();
			if(rs.next()) {
				return true;
			}
		}catch(Exception e) {
			e.printStackTrace();
			dbTools.closeAllAboutDB(conn, rs, pst);			
		}		
		return false;
	}
	
	@SuppressWarnings("unchecked")
	public List queryAll() {
		String sql = "select * from 'order'";
		return this.executeQuery(sql);
	}
	
	public String[] getAllSupplierName() {
		
		try{
			dbTools = new DBTools();
			conn = dbTools.getConnection();
			sql = "select supplier.supplier_name as supplierName from supplier";
			pst = conn.prepareStatement(sql);
			rs = pst.executeQuery();
			rs.last();			
			String[] claNames = new String[rs.getRow()+1];
			rs.first();
			int i = 2;
			claNames[0]="";
			if(rs.first())
				claNames[1]=rs.getString("supplierName");			
			while(rs.next()) {
				claNames[i] = rs.getString("supplierName");
				i++;
			}
			return claNames;
		}catch(Exception e) {
			e.printStackTrace();
			return null;
		}finally{
			dbTools.closeAllAboutDB(conn, rs, pst);
		}
	}
}
