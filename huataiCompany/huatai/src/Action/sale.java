package Action;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

import Form.inputs;
import dbTools.DBTools;

public class sale {
	DBTools dbTools = null;
	Connection conn;
	PreparedStatement pst;
	ResultSet rs;
	String sql="";
	public inputs getOneTeacher(int id) {
		try{
			dbTools = new DBTools();
			conn = dbTools.getConnection();
			sql = "select * from sale where sid=?";
			pst = conn.prepareStatement(sql);
			pst.setInt(1, id);
			rs = pst.executeQuery();
			if(rs.next()) {
				inputs input = new inputs();
				input.setInput_id(rs.getInt("sid"));
				input.setInput_name(rs.getString("sname"));
				input.setInput_num(rs.getDouble("per_price"));
				return input;
			}
		}catch(Exception e) {
			e.printStackTrace();
			dbTools.closeAllAboutDB(conn, rs, pst);			
		}		
		return null;
	}
	
	public boolean deleteOneTeacher(int id) {
		try{
			dbTools = new DBTools();
			conn = dbTools.getConnection();
			sql = "delete from sale where sid=?";
			pst = conn.prepareStatement(sql);
			pst.setInt(1, id);
			if(pst.executeUpdate() != 0) {
				return true;
			}			
		}catch(Exception e) {
			return false;					
		}finally{
			dbTools.closeAllAboutDB(conn, rs, pst);	
		}
		return false;
	}
	
	public boolean updateOneTeacher(inputs t) {
		try{
			dbTools = new DBTools();
			conn = dbTools.getConnection();
			sql = "update sale set sname=?,per_price=? where input_id=?";
			pst = conn.prepareStatement(sql);
			pst.setString(1, t.getInput_name());
			pst.setDouble(2, t.getInput_num());
			pst.setInt(3, t.getInput_id());
			if(pst.executeUpdate() != 0) {
				return true;
			}
		}catch(Exception e) {
			e.printStackTrace();
			dbTools.closeAllAboutDB(conn, rs, pst);			
		}		
		return false;
	}
	
	@SuppressWarnings("unchecked")
	public List executeQuery(String sql) {
		try{
			List inputs = new ArrayList();
			dbTools = new DBTools();
			conn = dbTools.getConnection();
			pst = conn.prepareStatement(sql);
			rs = pst.executeQuery(sql);
			while(rs.next()) {
				inputs t = new inputs();
				t.setInput_id(rs.getInt("sid"));
				t.setInput_name(rs.getString("sname"));
				t.setInput_num(rs.getDouble("per_price"));
				inputs.add(t);
			}
			return inputs;
		}catch(Exception e) {
			e.printStackTrace();
		}
		return null;
	}
	
	public boolean newTeacher(inputs t) {
		try{
			dbTools = new DBTools();
			conn = dbTools.getConnection();
			sql = "insert into sale(sid,sname,per_price)values(?,?,?)";
			pst = conn.prepareStatement(sql);
			pst.setInt(1, t.getInput_id());
			pst.setString(2, t.getInput_name());
			pst.setDouble(3, t.getInput_num());
			if(pst.executeUpdate() != 0) {
				return true;
			}
		}catch(Exception e) {
			e.printStackTrace();
			dbTools.closeAllAboutDB(conn, rs, pst);			
		}		
		return false;
	}
	
	public boolean isIdUsed(int id) {
		try{
			dbTools = new DBTools();
			conn = dbTools.getConnection();
			sql = "select * from sale where sid=?";
			pst = conn.prepareStatement(sql);
			pst.setInt(1, id);
			rs = pst.executeQuery();
			if(rs.next()) {
				return true;
			}
		}catch(Exception e) {
			e.printStackTrace();
			dbTools.closeAllAboutDB(conn, rs, pst);			
		}		
		return false;
	}
	
	@SuppressWarnings("unchecked")
	public List queryAll() {
		String sql = "select * from sale";
		return this.executeQuery(sql);
	}
}
