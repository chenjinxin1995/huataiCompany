package table;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Vector;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JSplitPane;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.ListSelectionModel;
import javax.swing.UIManager;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;

import Action.item;
import DefaultTable.itemDefaultTable;
import Form.items;
import table.itemtable;

public class itemframe extends JFrame implements ActionListener,ListSelectionListener{
	
	private static final long serialVersionUID = -2000035301302910482L;
	
	//数据库操作模块
	private JLabel id;
	private JTextField idText;
	private JLabel name;
	private JTextField nameText;
	private JLabel num;
	private JTextField numText;
	private JLabel price;
	private JTextField priceText;
	private JButton searchButton;
	private JButton createButton;
	private JButton deleteButton;
	private JButton changeButton;
	private JButton clearButton;
	private JButton cancel;
	private Container contentPane;
	private JPanel jPanel1;
	private JSplitPane jSplitPane;
	private JTable jTable;
	private JScrollPane jScrollPane;
	private ListSelectionModel selectionMode;
	
	private items teacher;
	@SuppressWarnings("unchecked")
	private Vector items;
	private int selected = 0;
	
	public itemframe() {
		try {
    //设置显示外观为本地系统外观，注意此句需放在初始化所有控件之前
		UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
		} catch (Exception e) {
			e.printStackTrace();
		}
		initComponents();
	}
	
	private void initComponents() {
		contentPane=getContentPane();
		jPanel1=new JPanel();
		jPanel1.setLayout(null);
		
		
		id=new JLabel();
		id.setText("商品号:");
		id.setFont(new Font("",Font.BOLD,15));
		id.setBounds(10, 20, 100, 20);
		jPanel1.add(id);
		
		idText=new JTextField();
		idText.setBounds(80, 20, 100, 20);
		jPanel1.add(idText);
		
		name=new JLabel();
		name.setText("商品名:");
		name.setFont(new Font("",Font.BOLD,15));
		name.setBounds(210, 20, 100, 20);
		jPanel1.add(name);		
		
		nameText=new JTextField();
		nameText.setBounds(280, 20, 100, 20);
		jPanel1.add(nameText);
		
		/*name=new JLabel();
		name.setText("数量:");
		name.setFont(new Font("",Font.BOLD,15));
		name.setBounds(410, 20, 100, 20);
		jPanel1.add(num);		
		
		nameText=new JTextField();
		nameText.setBounds(480, 20, 100, 20);
		jPanel1.add(numText);
		
		name=new JLabel();
		name.setText("价格:");
		name.setFont(new Font("",Font.BOLD,15));
		name.setBounds(10, 50, 100, 20);
		jPanel1.add(price);		
		
		nameText=new JTextField();
		nameText.setBounds(80, 50, 140, 20);
		jPanel1.add(priceText);*/
		
		searchButton=new JButton("查询");
		searchButton.setFont(new Font("",Font.BOLD,15));
		searchButton.setBounds(10, 70, 100, 30);
		searchButton.addActionListener(this);
		jPanel1.add(searchButton);
		
		createButton=new JButton("新建");
		createButton.setFont(new Font("",Font.BOLD,15));
		createButton.setBounds(130, 70, 100, 30);
		createButton.addActionListener(this);
		jPanel1.add(createButton);
		
		changeButton=new JButton("修改");
		changeButton.setFont(new Font("",Font.BOLD,15));
		changeButton.setBounds(250, 70, 100, 30);
		//changeButton.setEnabled(false);
		changeButton.addActionListener(this);
		jPanel1.add(changeButton);
		
		deleteButton=new JButton("删除");
		deleteButton.setFont(new Font("",Font.BOLD,15));
		deleteButton.setBounds(10, 120, 100, 30);
		//deleteButton.setEnabled(false);
		deleteButton.addActionListener(this);
		jPanel1.add(deleteButton);
		
		clearButton=new JButton("清空");
		clearButton.setFont(new Font("",Font.BOLD,15));
		clearButton.setBounds(130, 120, 100, 30);
		clearButton.addActionListener(this);
		jPanel1.add(clearButton);
		
		cancel=new JButton("退出");
		cancel.setFont(new Font("",Font.BOLD,15));
		cancel.setBounds(250, 120, 100, 30);
		cancel.addActionListener(this);
		jPanel1.add(cancel);
		
		jTable=new JTable(new itemDefaultTable(15,4));
		jScrollPane=new JScrollPane(jTable);
		
		jSplitPane=new JSplitPane(JSplitPane.VERTICAL_SPLIT,false,jPanel1,jScrollPane);
		jSplitPane.setDividerLocation(180);//表格的相对位置
		jSplitPane.setOneTouchExpandable(true);
		jSplitPane.setDividerSize(5);
		
		contentPane.add(jSplitPane);
		
		//Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
		Dimension screenSize = new Dimension(600, 500);//窗口大小
		this.setPreferredSize(screenSize);
		this.setBounds(screenSize.width/2-50,screenSize.height/2-80,500,500);
		this.setVisible(true);
		this.setTitle("查询界面");
		setResizable(false);
		pack();
	}

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// TODO 自动生成方法存根
		java.awt.EventQueue.invokeLater(new Runnable() {
			public void run() {
				new itemframe().setVisible(true);
			}
		});

	}
	
	//清空输入的查询条件
	private void clear(){
		idText.setText("");
		nameText.setText("");
		numText.setText("");
		priceText.setText("");
	}

	//捕捉到JButton点击事件后的实现的逻辑
	@SuppressWarnings("unchecked")
	public void actionPerformed(ActionEvent e) {
		item teacherAction = new item();
		// 点击"退出"按钮
		if(e.getActionCommand().equals("退出")){
			//退出系统
			//System.exit(0);
			this.dispose();
		}
		//点击"清空"按钮
		if(e.getActionCommand().equals("清空")){
			//清空输入的查询条件
			clear();
		}
		//点击"新建"按钮
		if(e.getActionCommand().equals("新建")){
			    //创建并进入新建模块
				//createFrame=new CreateFrame(this);
                if(idText.getText().equals("")||nameText.getText().equals("")) {
    				String message="商品号，商品名，均不能为空";
    				//显示提示框,提示用户
    				JOptionPane.showMessageDialog(this, message);
                }
                else if(!idText.getText().matches("[1-9][0-9]*")) {
                	String message="商品号必须为非零开始的数字";
    				//显示提示框,提示用户
    				JOptionPane.showMessageDialog(this, message);
                }
                else {
                	items c = new items();
    				c.setClass_id(Integer.valueOf(idText.getText()).intValue());
    				c.setClass_name(nameText.getText());
    				/*c.setClass_num(Integer.valueOf(numText.getText()).intValue());
    				c.setClass_price(Integer.valueOf(priceText.getText()).intValue());*/
    				if(teacherAction.isIdUsed(Integer.valueOf(idText.getText()).intValue())) {
    					String message="商品号不可用";
        				//显示提示框,提示用户
        				JOptionPane.showMessageDialog(this, message);
    				}
    				else {
    					if(teacherAction.newClasses(c)) {
                    		String message="商品信息新建成功";
            				//显示提示框,提示用户
            				JOptionPane.showMessageDialog(this, message);
            				this.afterCreate(c);
                    	}
    				}                	
                }
		}
		//点击"删除"按钮
		if(e.getActionCommand().equals("删除")){
			//若没有在查询功能界面中输入班级号，不允许进入删除界面
			if(idText.getText().equals("")){
				//未输入班级号的提示信息
				String message="删除时商品号不能为空";
				//显示提示框,提示用户
				JOptionPane.showMessageDialog(this, message);
			}
			else{
				int teache_id = Integer.valueOf(idText.getText()).intValue();
					if(teacherAction.deleteOneClasses(teache_id)) {
						//未输入班级号的提示信息
						String message="成功删除一条记录";
						//显示提示框,提示用户
						JOptionPane.showMessageDialog(this, message);
						this.afterDelete();
					}
					else{
						//未输入班级号的提示信息
						String message="未能成功删除一条记录";
						//显示提示框,提示用户
						JOptionPane.showMessageDialog(this, message);
					}			
			}
			
			
		}
		//点击"修改"按钮
		if(e.getActionCommand().equals("修改")){
			if(idText.getText().equals("")||"".equals(nameText.getText())
					){
				//需输入班级号的提示信息
				String message="修改内容均不能为空";
				//显示提示框,提示用户
				JOptionPane.showMessageDialog(this, message);
			}
			else{
				items c = new items();
				c.setClass_id(Integer.valueOf(idText.getText()).intValue());
				c.setClass_name(nameText.getText());
/*				c.setClass_num(Integer.valueOf(numText.getText()).intValue());
				c.setClass_price(Integer.valueOf(priceText.getText()).intValue());*/
				if(teacherAction.updateOneClasses(c)) {
					String message="更新成功";
					//显示提示框,提示用户
					JOptionPane.showMessageDialog(this, message);
					this.afterChange(c);					
				}
			}
		}
		//点击"查询"按钮
		if(e.getActionCommand().equals("查询")){
			//获得支持模糊查询的SQL语句
			String sql=getQuerySQL();
			//获得查询后的结果集
			List tchers = new ArrayList();
			tchers = teacherAction.executeQuery(sql);
			//若没有符合条件的记录，清空输入的查询条件
		    if(tchers.isEmpty()){
		    	clear();
		    	//没有查询到符合条件的班级的提示
		    	String message="没有符合条件的商品信息";
		    	//显示提示框,提示用户
				JOptionPane.showMessageDialog(this,message);
		    	return;
		    }		    
		    //将查询记录以Classes对象的形式放入查询结果Vector
				items=new Vector();
				Iterator iterator = tchers.iterator();
				while(iterator.hasNext()) {
					items.add(iterator.next());
				}
		    //更新查询界面
		    addComponents();
		    //获得代表第一个查询记录的Classes对象
		    teacher=(items)items.get(0);
		    //将该对象中的班级信息显示在查询功能界面中
			setClassesInformation(teacher);
			//将修改和删除按钮设置为可用	
			changeButton.setEnabled(true);			
			deleteButton.setEnabled(true);
		}
		
	}

	//更新查询界面
	private void addComponents() {
		// TODO 自动生成方法存根
		contentPane.removeAll();
		itemtable mt=new itemtable(items);
		jTable=new JTable(mt);
		selectionMode=jTable.getSelectionModel();
		selectionMode.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
	    selectionMode.addListSelectionListener(this);
        jScrollPane=new JScrollPane(jTable);
		
		jSplitPane=new JSplitPane(JSplitPane.VERTICAL_SPLIT,false,jPanel1,jScrollPane);
		jSplitPane.setDividerLocation(180);
		jSplitPane.setOneTouchExpandable(true);
		jSplitPane.setDividerSize(5);
		contentPane.add(jSplitPane);
		this.setTitle("查询界面");
		this.setVisible(true);
		setResizable(false);
		pack();
	}
	
	//新建班级信息后，更新查询界面
	@SuppressWarnings("unchecked")
	public void afterCreate(items t){
		if(items == null){
			items=new Vector();
		}
		items.add(t);
		addComponents();
	}
	
	//删除班级信息后，更新查询界面
	public void afterDelete(){
		items.remove(selected);
		addComponents();
	}
	
	//修改班级信息后，更新查询界面
	@SuppressWarnings("unchecked")
	public void afterChange(items t){
		items.remove(selected);
		items.add(selected,t);
		addComponents();
	}
	
	//将Classes对象包含的班级信息显示在查询功能界面的相应位置上
	private void setClassesInformation(items items2){
		idText.setText(items2.getClass_id()+"");
		nameText.setText(items2.getClass_name());
/*		numText.setText(items2.getClass_num()+"");
		priceText.setText(items2.getClass_price()+"");*/
	}

	//捕捉ListSelectionEvent事件后的处理逻辑
	public void valueChanged(ListSelectionEvent e) {
		selected = jTable.getSelectedRow();
		items selectedT = (items) items.get(selected);
		this.setClassesInformation(selectedT);
	}
	
    //获得支持模糊查询的SQL语句
	private String getQuerySQL(){
		String[] general={"%","%"};
		if(!idText.getText().equals("")){
			general[0]="%"+idText.getText()+"%";
		}
		if(!nameText.getText().equals("")){
			general[1]="%"+nameText.getText()+"%";
		}
		/*if(!numText.getText().equals("")){
			general[2]="%"+numText.getText()+"%";
		}
		if(!priceText.getText().equals("")){
			general[3]="%"+priceText.getText()+"%";
		}*/
		String query = "select * from items where item_id like'"+general[0]+"' and item_name like '"+general[1]+"' order by item_id";
	    return query;
	}

	

}
