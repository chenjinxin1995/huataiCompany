package table;

import java.util.Vector;

import javax.swing.table.AbstractTableModel;

import Form.orders;

public class orderframe extends AbstractTableModel{

	private static final long serialVersionUID = -8175947642595279176L;
	
	@SuppressWarnings("unchecked")
	private Vector content = null;

    private String[] columnName = { "订单号", "订单名", "数量", "供货商"};
    
    @SuppressWarnings("unchecked")
	public orderframe(Vector orders) {
    	content = new Vector(20);
    	int rows = orders.size();
		for(int i=0;i<rows;i++) {
			content.add(((orders)orders.get(i)).toVector());
		}
    }
    
	@Override
	public int getColumnCount() {
		// TODO Auto-generated method stub
		return columnName.length;
	}
	
	//获得相应列的列名
    public String getColumnName(int col){
    	return columnName[col];
    }
	
	@Override
	public int getRowCount() {
		// TODO Auto-generated method stub
		return this.content.size();
	}

	@SuppressWarnings("unchecked")
	public Object getValueAt(int rowIndex, int columnIndex) {
		// TODO 自动生成方法存根
		//System.out.println("tablemodel is ok");
		return ((Vector) content.get(rowIndex)).get(columnIndex);
	}
	
	//设置单元格不个编辑
	public boolean isCellEditable(int rowIndex,int columnIndex){
		return false;
	}
	
	/**
	 * 使修改的内容生效
	 */
	@SuppressWarnings("unchecked")
	public void setValueAt(Object value, int row, int col) {
        ((Vector) content.get(row)).remove(col);
        ((Vector) content.get(row)).add(col, value);
        this.fireTableCellUpdated(row, col);
    }

}
