package Form;

import java.util.Vector;

public class stores {
	private int store_id;
	private String store_name;
	private int store_snum;
	public int getStore_id() {
		return store_id;
	}
	public void setStore_id(int storeId) {
		store_id = storeId;
	}
	public String getStore_name() {
		return store_name;
	}
	public void setStore_name(String storeName) {
		store_name = storeName;
	}
	public int getStore_snum() {
		return store_snum;
	}
	public void setStore_snum(int storeSnum) {
		store_snum = storeSnum;
	}
	@SuppressWarnings("unchecked")
	public Vector toVector() {
		Vector v = new Vector(3);
        v.add(0, this.getStore_id());
        v.add(1, this.getStore_name());
        v.add(2,this.getStore_snum());
        return v;
	}
}
