package client;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.Vector;
import clientdao.*;
import clientUI.*;
public class clientImp implements client{
private String name;
private String contact;
private String money;
private String price;
private String note;
private String sign;
private Vector client;
public String getName() {
	return name;
}
public void setName(String name) {
	this.name = name;
}
public String getContact() {
	return contact;
}
public void setContact(String contact) {
	this.contact = contact;
}
public String getMoney() {
	return money;
}
public void setMoney(String money) {
	this.money = money;
}
public String getPrice() {
	return price;
}
public void setPrice(String price) {
	this.price = price;
}
public String getNote() {
	return note;
}
public void setNote(String note) {
	this.note = note;
}
public String getSign() {
	return sign;
}
public void setSign(String sign) {
	this.sign = sign;
}



public void add() {
	// TODO Auto-generated method stub
	clientdaoImp a=new clientdaoImp();
	clientUIImp b=new clientUIImp();

	if(a.addsql(getName(),getContact(),getNote(),getSign())){
		b.successUI();
	}
	else{
		b.failUI();
	}

}

public void change() {
	// TODO Auto-generated method stub
	clientdaoImp a=new clientdaoImp();
	clientUIImp b=new clientUIImp();

	if(a.changesql(getName(),getContact(),getNote(),getSign(),getMoney(),getPrice())){
		b.successUI();
	}
	else{
		b.failUI();
	}
}

public Vector search() {
	// TODO Auto-generated method stub
	ArrayList b=new ArrayList();
	clientdaoImp a=new clientdaoImp();
	b=a.searchsql();
	return clienttable(b);
}

public Vector clienttable(ArrayList clientinf){
	client=new Vector();
	Iterator iterator = clientinf.iterator();
	while(iterator.hasNext()) {
		client.add(iterator.next());
	}
	return client;
	
}
public void repay1() {
	clientdaoImp a=new clientdaoImp();
	clientUIImp b=new clientUIImp();
	if(a.repay1(getName(),getMoney())){
		b.successUI();
	}
	else{
		b.failUI();
	}
}

public void repay2() {
	clientdaoImp a=new clientdaoImp();
	clientUIImp b=new clientUIImp();
	if(a.repay2(getName(),getMoney())){
		b.successUI();
	}
	else{
		b.failUI();
	}
}

public void end() {
	// TODO Auto-generated method stub
	
}

public void addinput(String name,String contact,String sign,String note) {
	// TODO Auto-generated method stub
	setName(name);
	setContact(contact);
	setSign(sign);
	setNote(note);
	add();
}

public void changeinput(String name,String contact,String money,String price,String note,String sign) {
	// TODO Auto-generated method stub

	setName(name);
	setContact(contact);
	setNote(note);
	setSign(sign);
	setMoney(money);
	setPrice(price);
	change();
	
}

public void repayinput1(String name,String money) {
	// TODO Auto-generated method stub
	clientUIImp b=new clientUIImp();
	setName(name);
	setMoney(money);
	repay1();
}
public void repayinput2(String name,String money) {
	// TODO Auto-generated method stub
	clientUIImp b=new clientUIImp();
	setName(name);
	setMoney(money);
	repay2();
}



}
