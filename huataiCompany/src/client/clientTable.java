package client;

import java.util.Vector;

import javax.swing.table.AbstractTableModel;

import client.*;

public class clientTable extends AbstractTableModel{
	private Vector content = null;

    private String[] columnName = { "姓名", "电话", "钱款","上次交易价格"};
    @SuppressWarnings("unchecked")
	public clientTable(Vector clientinformation) {
    	content = new Vector(10);
    	int rows = clientinformation.size();
		for(int i=0;i<rows;i++) {
			content.add(((clientinf)clientinformation.get(i)).toVector());
		}
    }
	
	@Override
	public int getColumnCount() {
		// TODO Auto-generated method stub
		return columnName.length;
	}
	
	
    public String getColumnName(int col){
    	return columnName[col];
    }

	@Override
	public int getRowCount() {
		// TODO Auto-generated method stub
		return this.content.size();
	}

	@Override
	public Object getValueAt(int rowIndex, int columnIndex) {
		
		return ((Vector) content.get(rowIndex)).get(columnIndex);
	}
	public void setValueAt(Object value, int row, int col) {
        ((Vector) content.get(row)).remove(col);
        ((Vector) content.get(row)).add(col, value);
        this.fireTableCellUpdated(row, col);
    }

}
