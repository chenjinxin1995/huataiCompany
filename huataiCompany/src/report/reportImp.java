package report;
import java.util.ArrayList;

import reportUI.*;
import reportdao.reportdaoImp;
import inputvalid.*;
import bigUI.*;
public class reportImp implements report{
	private String year;
	private String year1;
	private String month1;
	private String year2;
	private String month2;
	public String getYear() {
		return year;
	}


	public void setYear(String year) {
		this.year = year;
	}


	public String getYear1() {
		return year1;
	}


	public void setYear1(String year1) {
		this.year1 = year1;
	}


	public String getMonth1() {
		return month1;
	}


	public void setMonth1(String month1) {
		this.month1 = month1;
	}


	public String getYear2() {
		return year2;
	}


	public void setYear2(String year2) {
		this.year2 = year2;
	}


	public String getMonth2() {
		return month2;
	}


	public void setMonth2(String month2) {
		this.month2 = month2;
	}



	public boolean inputvalid1() {
		// TODO Auto-generated method stub
		String year=getYear();
		inputvalidImp a=new inputvalidImp();
		return a.inputStringvalid(year);
	}

	
	public boolean inputvalid2() {
		// TODO Auto-generated method stub
		String year1=getYear1();
		String month1=getMonth1();
		String year2=getYear2();
		String month2=getMonth2();
		int realyear[]=new int[4];
		try{
			 realyear[0]=Integer.parseInt(year1);
			 realyear[1]=Integer.parseInt(month1);
			 realyear[2]=Integer.parseInt(year2);
			 realyear[3]=Integer.parseInt(month2);
			 return true;
		}
		catch (Exception e){
			inputvalidUIImp a=new inputvalidUIImp();
			a.inputvalidshow();
		}
		return false;
	}


	public ArrayList getyear1(String year) {
		setYear(year);
		ArrayList a=new ArrayList();
		if(inputvalid1()){
		reportdaoImp b=new reportdaoImp();
		a=b.report1(getYear());
		}
		else {
		inputvalidUIImp c=new inputvalidUIImp();
		c.inputvalidshow();
		}
		return a;
	}
	public ArrayList getyear2(String year) {
		setYear(year);
		ArrayList a=new ArrayList();
		if(inputvalid1()){
		reportdaoImp b=new reportdaoImp();
		a=b.report2(getYear());
		}
		else {
		inputvalidUIImp c=new inputvalidUIImp();
		c.inputvalidshow();
		}
		return a;
	}

	
}
