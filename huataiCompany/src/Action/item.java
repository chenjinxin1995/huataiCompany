package Action;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

import Form.items;
import Form.orders;
import dbTools.DBTools;

public class item {
	DBTools dbTools = null;
	Connection conn;
	PreparedStatement pst;
	ResultSet rs;
	String sql="";
	public items getOneClasses(int id) {
		try{
			dbTools = new DBTools();
			conn = dbTools.getConnection();
			sql = "select * from items where item_id=?";
			pst = conn.prepareStatement(sql);
			pst.setInt(1, id);
			rs = pst.executeQuery();

			if(rs.next()) {
				items items = new items();
				items.setClass_id(rs.getInt("item_id"));
				items.setClass_name(rs.getString("item_name"));
				items.setClass_num(rs.getInt("warning_number"));
				items.setClass_price(rs.getInt("item_price"));
				return items;
			}
			
		}catch(Exception e) {
			e.printStackTrace();
			dbTools.closeAllAboutDB(conn, rs, pst);			
		}		
		return null;
	}
	
	public boolean deleteOneClasses(int id) {
		try{
			dbTools = new DBTools();
			conn = dbTools.getConnection();
			sql = "delete from items where item_id=?";
			pst = conn.prepareStatement(sql);
			pst.setInt(1, id);
			if(pst.executeUpdate() != 0) {
				return true;
			}			
		}catch(Exception e) {
			return false;					
		}finally{
			dbTools.closeAllAboutDB(conn, rs, pst);	
		}
		return false;
	}
	
	public boolean updateOneClasses(items t) {
		try{
			dbTools = new DBTools();
			conn = dbTools.getConnection();
			sql = "update items set item_name=?,warning_number=?,item_price=? where item_id=?";
			pst = conn.prepareStatement(sql);
			pst.setString(1, t.getClass_name());
			pst.setInt(2, t.getClass_num());
			pst.setInt(3, t.getClass_price());
			pst.setInt(4, t.getClass_id());
			if(pst.executeUpdate() != 0) {
				return true;
			}
		}catch(Exception e) {
			e.printStackTrace();
			dbTools.closeAllAboutDB(conn, rs, pst);			
		}		
		return false;
	}
	
	@SuppressWarnings("unchecked")
	public List executeQuery(String sql) {
		try{
			List itemss = new ArrayList();
			dbTools = new DBTools();
			conn = dbTools.getConnection();
			pst = conn.prepareStatement(sql);
			rs = pst.executeQuery(sql);
			while(rs.next()) {
				items t = new items();
				t.setClass_id(rs.getInt("item_id"));
				t.setClass_name(rs.getString("item_name"));
				t.setClass_num(rs.getInt("warning_number"));
				t.setClass_price(rs.getInt("item_price"));
				itemss.add(t);
			}
			return itemss;
		}catch(Exception e) {
			e.printStackTrace();
		}
		return null;
	}
	
	public boolean newClasses(items t) {
		try{
			dbTools = new DBTools();
			conn = dbTools.getConnection();
			sql = "insert into items(item_id,item_name,warning_number,item_price)values(?,?,?,?)";
			pst = conn.prepareStatement(sql);
			pst.setInt(1, t.getClass_id());
			pst.setString(2, t.getClass_name());
			pst.setInt(3, t.getClass_num());
			pst.setInt(4, t.getClass_price());
			System.out.print(t.getClass_name());
			if(pst.executeUpdate() != 0) {
				return true;
			}
		}catch(Exception e) {
			e.printStackTrace();
			dbTools.closeAllAboutDB(conn, rs, pst);			
		}		
		return false;
	}
	
	public boolean isIdUsed(int id) {
		try{
			dbTools = new DBTools();
			conn = dbTools.getConnection();
			sql = "select * from items where item_id=?";
			pst = conn.prepareStatement(sql);
			pst.setInt(1, id);
			rs = pst.executeQuery();
			if(rs.next()) {
				return true;
			}
		}catch(Exception e) {
			e.printStackTrace();
			dbTools.closeAllAboutDB(conn, rs, pst);			
		}		
		return false;
	}
	
	@SuppressWarnings("unchecked")
	public List queryAll() {
		String sql = "select * from items";
		return this.executeQuery(sql);
	}
}
