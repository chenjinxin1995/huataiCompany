package Action;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

import Form.stores;
import dbTools.DBTools;
import clientUI.*;
public class store {
	DBTools dbTools = null;
	Connection conn;
	PreparedStatement pst;
	ResultSet rs;
	String sql="";
	public stores getOneCourse(int id) {
		try{
			dbTools = new DBTools();
			conn = dbTools.getConnection();
			sql = "select * from store where store_id=?";
			pst = conn.prepareStatement(sql);
			pst.setInt(1, id);
			rs = pst.executeQuery();
			if(rs.next()) {
				stores store = new stores();
				store.setStore_id(rs.getInt("store_id"));
				store.setStore_name(rs.getString("store_name"));
				store.setStore_snum(rs.getInt("store_number"));
				return store;
			}
		}catch(Exception e) {
			clientUIImp a=new clientUIImp();
			a.failUI();
			dbTools.closeAllAboutDB(conn, rs, pst);			
		}		
		return null;
	}
	
	public boolean deleteOneCourse(int id) {
		try{
			dbTools = new DBTools();
			conn = dbTools.getConnection();
			sql = "delete from store where store_id=?";
			pst = conn.prepareStatement(sql);
			pst.setInt(1, id);
			if(pst.executeUpdate() != 0) {
				return true;
			}			
		}catch(Exception e) {
			return false;					
		}finally{
			dbTools.closeAllAboutDB(conn, rs, pst);	
		}
		return false;
	}
	
	public boolean updateOneCourse(stores t) {
		try{
			dbTools = new DBTools();
			conn = dbTools.getConnection();
			sql = "update store set store_name=?,store_number=? where store_id=?";
			pst = conn.prepareStatement(sql);
			pst.setString(1, t.getStore_name());
			pst.setInt(2, t.getStore_snum());
			pst.setInt(3, t.getStore_id());
			if(pst.executeUpdate() != 0) {
				return true;
			}
		}catch(Exception e) {
			e.printStackTrace();
			dbTools.closeAllAboutDB(conn, rs, pst);			
		}		
		return false;
	}
	
	@SuppressWarnings("unchecked")
	public List executeQuery(String sql) {
		try{
			List stores = new ArrayList();
			dbTools = new DBTools();
			conn = dbTools.getConnection();
			pst = conn.prepareStatement(sql);
			rs = pst.executeQuery(sql);
			while(rs.next()) {
				stores t = new stores();
				t.setStore_id(rs.getInt("store_id"));
				t.setStore_name(rs.getString("store_name"));
				t.setStore_snum(rs.getInt("store_number"));
				stores.add(t);
			}
			return stores;
		}catch(Exception e) {
			e.printStackTrace();
		}
		return null;
	}
	
	public boolean newCourse(stores t) {
		try{
			dbTools = new DBTools();
			conn = dbTools.getConnection();
			sql = "insert into store(store_id,store_name,store_number)values(?,?,?)";
			pst = conn.prepareStatement(sql);
			pst.setInt(1, t.getStore_id());
			pst.setString(2, t.getStore_name());
			pst.setInt(3, t.getStore_snum());
			if(pst.executeUpdate() != 0) {
				return true;
			}
		}catch(Exception e) {
			clientUIImp a=new clientUIImp();
			a.failUI();
			dbTools.closeAllAboutDB(conn, rs, pst);			
		}		
		return false;
	}
	
	public boolean isIdUsed(int id) {
		try{
			dbTools = new DBTools();
			conn = dbTools.getConnection();
			sql = "select * from store where store_id=?";
			pst = conn.prepareStatement(sql);
			pst.setInt(1, id);
			rs = pst.executeQuery();
			if(rs.next()) {
				return true;
			}
		}catch(Exception e) {
			e.printStackTrace();
			dbTools.closeAllAboutDB(conn, rs, pst);			
		}		
		return false;
	}
	
	@SuppressWarnings("unchecked")
	public List queryAll() {
		String sql = "select * from store";
		return this.executeQuery(sql);
	}
}
