package table;

import java.util.Vector;

import javax.swing.table.AbstractTableModel;

import Form.inputs;

public class saleframe extends AbstractTableModel{

	private static final long serialVersionUID = -8175947642595279176L;
	
	@SuppressWarnings("unchecked")
	private Vector content = null;

    private String[] columnName = { "生产号", "生产名", "产量"};
    
    @SuppressWarnings("unchecked")
	public saleframe(Vector inputs) {
    	content = new Vector(20);
    	int rows = inputs.size();
		for(int i=0;i<rows;i++) {
			content.add(((inputs)inputs.get(i)).toVector());
		}
    }
    
	@Override
	public int getColumnCount() {
		// TODO Auto-generated method stub
		return columnName.length;
	}
	
	//获得相应列的列名
    public String getColumnName(int col){
    	return columnName[col];
    }
	
	@Override
	public int getRowCount() {
		// TODO Auto-generated method stub
		return this.content.size();
	}

	@SuppressWarnings("unchecked")
	public Object getValueAt(int rowIndex, int columnIndex) {
		// TODO 自动生成方法存根
		//System.out.println("tablemodel is ok");
		return ((Vector) content.get(rowIndex)).get(columnIndex);
	}
	
	//设置单元格不个编辑
	public boolean isCellEditable(int rowIndex,int columnIndex){
		return false;
	}
	
	/**
	 * 使修改的内容生效
	 */
	@SuppressWarnings("unchecked")
	public void setValueAt(Object value, int row, int col) {
        ((Vector) content.get(row)).remove(col);
        ((Vector) content.get(row)).add(col, value);
        this.fireTableCellUpdated(row, col);
    }

}
