package Form;

import java.util.Vector;

public class items {
	private int item_id;
	private String item_name;
	private int item_num;
	private int item_price;
	public int getClass_num() {
		return item_num;
	}
	public void setClass_num(int item_Num) {
		item_num = item_Num;
	}
	public int getClass_price() {
		return item_price;
	}
	public void setClass_price(int item_Price) {
		item_price = item_Price;
	}


	public int getClass_id() {
		return item_id;
	}
	public void setClass_id(int itemId) {
		item_id = itemId;
	}
	public String getClass_name() {
		return item_name;
	}
	public void setClass_name(String itemName) {
		item_name = itemName;
	}
	
	@SuppressWarnings("unchecked")
	public Vector toVector() {
		Vector v = new Vector(2);
        v.add(0, this.getClass_id());
        v.add(1, this.getClass_name());
        v.add(2, this.getClass_num());
        v.add(3, this.getClass_price());
        return v;
	}
}
