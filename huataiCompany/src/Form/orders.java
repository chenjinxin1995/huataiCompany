package Form;

import java.util.Vector;

public class orders {
	private int order_id;
	private String order_name;
	private int order_num;
	private String supplier_name;
	private String stu_class_name;
	public int getOrder_id() {
		return order_id;
	}
	public void setOrder_id(int orderId) {
		order_id = orderId;
	}
	public String getOrder_name() {
		return order_name;
	}
	public void setOrder_name(String orderName) {
		order_name = orderName;
	}
	public int getOrder_num() {
		return order_num;
	}
	public void setOrder_num(int orderNum) {
		order_num = orderNum;
	}
	public String getSupplier_name() {
		return supplier_name;
	}
	public void setSupplier_name(String supplierName) {
		supplier_name =supplierName;
	}
	
	public String getStu_class_name() {
		return stu_class_name;
	}
	public void setStu_class_name(String stuClassName) {
		stu_class_name = stuClassName;
	}
	@SuppressWarnings("unchecked")
	public Vector toVector() {
		Vector v = new Vector(4);
        v.add(0, this.getOrder_id());
        v.add(1, this.getOrder_name());
        v.add(2, this.getOrder_num());
        v.add(3, this.getSupplier_name());
        return v;
	}
}
