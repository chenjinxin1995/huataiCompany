package Form;

import java.util.Vector;

public class inputs {
	private int input_id;
	private String input_name;
	private double per_price;
	public int getInput_id() {
		return input_id;
	}
	public void setInput_id(int inputId) {
		input_id = inputId;
	}
	public String getInput_name() {
		return input_name;
	}
	public void setInput_name(String inputName) {
		input_name = inputName;
	}
	public double getInput_num() {
		return per_price;
	}
	public void setInput_num(double d) {
		per_price = d;
	}
	@SuppressWarnings("unchecked")
	public Vector toVector() {
		Vector v = new Vector(3);
        v.add(0, this.getInput_id());
        v.add(1, this.getInput_name());
        v.add(2, this.getInput_num());
        return v;
	}
}
